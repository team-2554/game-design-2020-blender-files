import random
import time
import cv2
import numpy as np


class MazeWall:
    def __init__(self):
        self.exists = True

    def destroy(self):
        self.exists = False

    def build(self):
        self.exists = True


class MazeCell:
    def __init__(self, northWall, eastWall, westWall, southWall):
        self.visited = False

        self.northWall = northWall
        self.eastWall = eastWall
        self.westWall = westWall
        self.southWall = southWall

    def visit(self):
        self.visited = True


class Maze:
    def __init__(self, w, h):
        self.w = w
        self.h = h

        cells = []

        for i in range(h):
            row = []

            for j in range(w):
                row.append(
                    MazeCell(
                        MazeWall() if i == 0 else cells[-1][j].southWall,
                        MazeWall(),
                        MazeWall() if j == 0 else row[-1].eastWall,
                        MazeWall(),
                    )
                )

            cells.append(row)

        self.cells = cells

    def build_frc(self, seed=-1):
        self.build(seed)

        self.cells[3][2].southWall.destroy()
        self.cells[3][2].eastWall.destroy()

        self.cells[3][3].southWall.destroy()

        self.cells[3][4].southWall.destroy()
        self.cells[3][4].westWall.destroy()

        self.cells[4][2].eastWall.destroy()
        self.cells[4][4].westWall.destroy()

        self.cells[random.choice([0, 1])][6].eastWall.destroy()
        self.cells[random.choice([6, 7])][0].westWall.destroy()

        materialAreaOpeningChoices = list(range(2, 6))
        random.shuffle(materialAreaOpeningChoices)
        self.cells[materialAreaOpeningChoices[0]][0].westWall.destroy()
        self.cells[materialAreaOpeningChoices[1]][0].westWall.destroy()

        random.shuffle(materialAreaOpeningChoices)
        self.cells[materialAreaOpeningChoices[0]][6].eastWall.destroy()
        self.cells[materialAreaOpeningChoices[1]][6].eastWall.destroy()

    def clampx(self, x):
        return max(0, min(x, self.w - 1))

    def clampy(self, y):
        return max(0, min(y, self.h - 1))

    def build(self, seed=-1):
        if seed == -1:
            seed = time.time()
        random.seed(seed)

        def walk(x, y):
            self.cells[y][x].visit()

            dirs = [
                (self.clampx(x - 1), y),
                (x, self.clampy(y + 1)),
                (self.clampx(x + 1), y),
                (x, self.clampy(y - 1)),
            ]
            random.shuffle(dirs)

            for (xx, yy) in dirs:
                if self.cells[yy][xx].visited:
                    continue
                if xx == x:
                    if yy > y:
                        self.cells[y][x].southWall.destroy()
                    elif yy < y:
                        self.cells[y][x].northWall.destroy()
                elif yy == y:
                    if xx > x:
                        self.cells[y][x].eastWall.destroy()
                    elif xx < x:
                        self.cells[y][x].westWall.destroy()
                walk(xx, yy)

        walk(random.randrange(self.w), random.randrange(self.h))

    def to_string_old(self):
        out = ""
        for i, row in enumerate(self.cells):
            for j, cell in enumerate(row):
                cell_num = (
                    (0b1000 if cell.northWall.exists else 0b0000)
                    | (0b0100 if cell.eastWall.exists else 0b0000)
                    | (0b0010 if cell.westWall.exists else 0b0000)
                    | (0b0001 if cell.southWall.exists else 0b0000)
                )
                cell_num_str = format(cell_num, "04b")

                out += f"{cell_num_str}" + ("" if j == self.w - 1 else "-")
            out += "\n"
            out += "" if i == self.h - 1 else (" ||  " * self.w + "\n")
        return out

    def to_string(self):
        out = ""
        for row in self.cells:
            for cell in row:
                if cell.northWall.exists:
                    out += "+---"
                else:
                    out += "+   "
            out += "+\n"

            for cell in row:
                if cell.westWall.exists:
                    out += "|   "
                else:
                    out += "    "
            if row[-1].eastWall.exists:
                out += "|"
            out += "\n"

        for cell in self.cells[-1]:
            if cell.southWall.exists:
                out += "+---"
            else:
                out += "+   "
        out += "+\n"

        return out

    def to_image(self, box_size=100, line_thickness_ratio=0.04):
        line_thickness = int(box_size * line_thickness_ratio)
        img = (
            np.zeros(
                (
                    box_size * self.h + line_thickness,
                    box_size * self.w + line_thickness,
                ),
                dtype=np.uint8,
            )
            * 255
        )
        h_line_thickness = line_thickness // 2

        def line(start, end):
            startx = start[0]
            starty = start[1]

            endx = end[0]
            endy = end[1]

            cv2.rectangle(
                img,
                (startx - h_line_thickness, starty - h_line_thickness),
                (endx + h_line_thickness - 1, endy + h_line_thickness - 1),
                (255, 255, 255),
                -1,
            )

        for y, row in enumerate(self.cells):
            for x, cell in enumerate(row):
                top_left = (
                    x * box_size + h_line_thickness,
                    y * box_size + h_line_thickness,
                )
                top_right = (
                    (x + 1) * box_size + h_line_thickness,
                    y * box_size + h_line_thickness,
                )
                bottom_left = (
                    x * box_size + h_line_thickness,
                    (y + 1) * box_size + h_line_thickness,
                )
                bottom_right = (
                    (x + 1) * box_size + h_line_thickness,
                    (y + 1) * box_size + h_line_thickness,
                )

                if cell.northWall.exists:
                    line(top_left, top_right)
                if cell.eastWall.exists:
                    line(top_right, bottom_right)
                if cell.westWall.exists:
                    line(top_left, bottom_left)
                if cell.southWall.exists:
                    line(bottom_left, bottom_right)
        return img


seeds = [
    2554,
    254,
    118,
    148,
    1678,
    1676,
    303,
    2590,
]

for i, seed in enumerate(seeds):
    maze = Maze(7, 8)

    maze.build_frc(seed)

    print("\n\n================================================\n\n")
    print(maze.to_string())

    img = maze.to_image()
    # cv2.imshow("maze", img)
    # cv2.waitKey(0)

    img = cv2.resize(img, (1024, 1024))
    cv2.imwrite(f"mazes/maze{i}.png", img)

# grid = Maze(7, 8)
# img = grid.to_image()

# img = cv2.resize(img, (1024, 1024))
# cv2.imwrite("mazes/grid.png", img)
